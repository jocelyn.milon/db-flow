package fr.afpa.dbflow.database;



import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.UUID;

@Table(database = AppDatabase.class)
public class User extends BaseModel {

  @PrimaryKey(autoincrement = true) // at least one primary key required
    public  int id;

  @Column
  public String name;

  @Column
 public int age;

  @Column
  public String pseudo;
}