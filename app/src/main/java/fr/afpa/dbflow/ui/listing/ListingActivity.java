package fr.afpa.dbflow.ui.listing;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.List;

import fr.afpa.dbflow.R;
import fr.afpa.dbflow.database.User;

public class ListingActivity extends AppCompatActivity {
    private ListView ListViewUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing);

        ListViewUser = (ListView) findViewById(R.id.ListViewUser);


        List<User>userTableList = SQLite.select()
                .from(User.class)
                .queryList();

        for(User user:userTableList){
            Log.e("query", "user name:" +user.name );
        }
    }
}
