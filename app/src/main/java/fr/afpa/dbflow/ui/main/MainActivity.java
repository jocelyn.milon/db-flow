package fr.afpa.dbflow.ui.main;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import fr.afpa.dbflow.R;
import fr.afpa.dbflow.database.User;

public class MainActivity extends AppCompatActivity {
    private EditText inputName;
    private EditText inputAge;
    private EditText inputID;
    private Button inscriptionButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        inputName = (EditText) findViewById(R.id.inputName);
        inputAge = (EditText) findViewById(R.id.inputAge);
        inputID = (EditText) findViewById(R.id.inputID);
        inscriptionButton = (Button) findViewById(R.id.inscriptionButton);

        inscriptionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // check name
                if(inputName.getText().toString().isEmpty())
                {
                    Toast.makeText(MainActivity.this, "vous devez saisir votre nom", Toast.LENGTH_LONG).show();
                    return;
                }

                // check age
                if(inputAge.getText().toString().isEmpty()){
                    Toast.makeText(MainActivity.this, "vous devez saisir votre age", Toast.LENGTH_LONG).show();
                    return;
                }
// check age
                int age = Integer.valueOf(inputAge.getText().toString());
                if (age<1 || age >150){
                    Toast.makeText(MainActivity.this, "vous devez saisir votre age reel", Toast.LENGTH_LONG).show();
                    return;
                }
                // check age numerique (java isnumeric sur stackflow
                if (!isNumeric(inputAge.getText().toString())){
                    Toast.makeText(MainActivity.this, "vous devez saisir votre age", Toast.LENGTH_LONG).show();
                    return;
                }
                User user1 = new User();
                user1.name = inputName.getText().toString();
                user1.age = Integer.valueOf(inputAge.getText().toString());
                user1.save();

                if (user1.save()){
                    inputName.getText().clear();
                    inputAge.setText(null);
                    inputID.setText("");

                    Toast.makeText(MainActivity.this, "information enregistré", Toast.LENGTH_LONG).show();
                    return;
                }
                Toast.makeText(MainActivity.this, "erreur d'enregistrement", Toast.LENGTH_LONG).show();
                return;
            }
        });

    }
    public boolean isNumeric(String s) {
        return s != null && s.matches("[-+]?\\d*\\.?\\d+");
    }

}
